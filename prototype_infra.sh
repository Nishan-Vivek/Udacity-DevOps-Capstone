#!/bin/bash
aws cloudformation deploy \
--stack-name prototype \
--template-file infra.yml \
--capabilities CAPABILITY_IAM \
--parameter-overrides  EnvironmentName=prototype VpcCIDR=10.1.0.0/16 \
PublicSubnetACIDR=10.1.0.0/24 PrivateSubnetACIDR=10.1.1.0/24