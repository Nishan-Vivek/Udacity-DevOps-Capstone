Description: >
  Nishan Vivekanandan / For Udacity Cloud DevOps Capstone
  This template deploys the following:
            - VPC with public and private subnet
            - IGW, NAT gateway and default routes for the private and public subnets. 
            - Jenkins instance in private subnet            - 
            - Necessary  routes, security groups, roles and permissions.
            

#============================================================
# Parameters
#============================================================

Parameters:

# General Parameters
  EnvironmentName:
        Description: An environment name that will be prefixed to resource names
        Type: String    

# Network parameters
  VpcCIDR: 
      Description: Please enter the IP range (CIDR notation) for this VPC
      Type: String
      Default: 10.0.0.0/16

  PublicSubnetACIDR:
      Description: Please enter the IP range (CIDR notation) for the public subnet in the first Availability Zone
      Type: String
      Default: 10.0.0.0/24

  PrivateSubnetACIDR:
      Description: Please enter the IP range (CIDR notation) for the private subnet in the first Availability Zone
      Type: String
      Default: 10.0.2.0/24

#============================================================
# Resources
#============================================================

#============================================================
# Network Configuration
#============================================================

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  InternetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName
  
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway

  PublicSubnetA: 
    Type: AWS::EC2::Subnet
    Properties:
        VpcId: !Ref VPC
        AvailabilityZone: !Select [ 0, !GetAZs '' ]
        CidrBlock: !Ref PublicSubnetACIDR
        MapPublicIpOnLaunch: true
        Tags: 
            - Key: Name 
              Value: !Sub ${EnvironmentName} Public Subnet (A)

  PrivateSubnetA: 
    Type: AWS::EC2::Subnet
    Properties:
        VpcId: !Ref VPC
        AvailabilityZone: !Select [ 0, !GetAZs '' ]
        CidrBlock: !Ref PrivateSubnetACIDR
        MapPublicIpOnLaunch: false
        Tags: 
            - Key: Name 
              Value: !Sub ${EnvironmentName} Private Subnet (A)


  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentName} Public Routes

  DefaultPublicRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  PublicSubnetARouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetA
      RouteTableId: !Ref PublicRouteTable

  NatGatewayAEIP:
      Type: AWS::EC2::EIP
      DependsOn: AttachGateway
      Properties:
        Domain: vpc
        Tags:
          - Key: Name
            Value: !Sub ${EnvironmentName} NatGatewayAEIP

  NatGatewayA:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayAEIP.AllocationId
      SubnetId: !Ref PublicSubnetA
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentName} NatGatewayA

  PrivateRouteTableA:
    Type: AWS::EC2::RouteTable
    Properties: 
        VpcId: !Ref VPC
        Tags: 
            - Key: Name 
              Value: !Sub ${EnvironmentName} Private Routes (A)

  DefaultPrivateRouteA:
      Type: AWS::EC2::Route
      Properties:
          RouteTableId: !Ref PrivateRouteTableA
          DestinationCidrBlock: 0.0.0.0/0
          NatGatewayId: !Ref NatGatewayA

  PrivateSubnetARouteTableAssociation:
      Type: AWS::EC2::SubnetRouteTableAssociation
      Properties:
          RouteTableId: !Ref PrivateRouteTableA
          SubnetId: !Ref PrivateSubnetA